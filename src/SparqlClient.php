<?php

namespace Tptools;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use RuntimeException;
use Wikibase\DataModel\Entity\EntityId;
use Wikibase\DataModel\Entity\EntityIdParser;

class SparqlClient {

	private $client;
	private $entityUriParser;

	public function __construct(
		$endpoint = 'https://query.wikidata.org/sparql',
		EntityIdParser $entityUriParser = null
	) {
		$this->client = new Client( [
			'base_uri' => $endpoint,
			'headers' => [
				'User-Agent' => 'TptTools by User:Tpt'
			]
		] );
		$this->entityUriParser = $entityUriParser ?: ( new WikidataUtils() )->newEntityUriParser();
	}

	public function getTuples( string $query ): array {
		try {
			$sparqlResponse = $this->client->post( '?format=json', [
				'form_params' => [
					'query' => $query
				]
			] );
		} catch ( RequestException $e ) {
			throw new RuntimeException(
				"Error while evaluating SPARQL query '$query': {$e->getMessage()}", 0, $e
			);
		}
		$sparqlArray = json_decode( $sparqlResponse->getBody(), true );
		return array_map( static function ( $binding ) {
			return array_map( static function ( $value ) {
				if ( array_key_exists( 'datatype', $value ) ) {
					switch ( $value['datatype'] ) {
						case 'http://www.w3.org/2001/XMLSchema#boolean':
							return $value['value'] === 'true' || $value['value'] === '1';
						case 'http://www.w3.org/2001/XMLSchema#double':
						case 'http://www.w3.org/2001/XMLSchema#float':
							return (float)$value['value'];
						case 'http://www.w3.org/2001/XMLSchema#integer':
							return (int)$value['value'];
						default:
							return $value['value'];
					}
				} else {
					return $value['value'];
				}
			}, $binding );
		},  $sparqlArray['results']['bindings'] );
	}

	/**
	 * @param string $queryWhereClose with ?entity the selected variable
	 * @return EntityId[]
	 */
	public function getEntityIds(
		string $queryWhereClose, int $limit = 100, int $offset = 0
	): array {
		$query = 'SELECT ?entity WHERE { ' . $queryWhereClose .
			' } LIMIT ' . $limit . ' OFFSET ' . $offset;
		return array_map( function ( $tuple ) {
			return $this->entityUriParser->parse( $tuple['entity'] );
		}, $this->getTuples( $query ) );
	}

	/**
	 * @param string $queryWhereClose with ?entity the selected variable
	 */
	public function countEntities( string $queryWhereClose ): ?int {
		$query = 'SELECT (COUNT(?entity) AS ?c) WHERE { ' . $queryWhereClose . ' }';
		$result = $this->getTuples( $query );
		return empty( $result ) ? null : end( $result )['c'];
	}
}

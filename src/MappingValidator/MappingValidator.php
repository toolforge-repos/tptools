<?php

namespace Tptools\MappingValidator;

use ARC2;
use Psr\SimpleCache\CacheInterface;
use Tptools\SparqlClient;

class MappingValidator {

	private const RDF_PROPERTY = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#Property';
	private const RDF_TYPE = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#type';
	private const RDFS_CLASS = 'http://www.w3.org/2000/01/rdf-schema#Class';
	private const RDFS_SUB_CLASS_OF = 'http://www.w3.org/2000/01/rdf-schema#subClassOf';
	private const RDFS_SUB_PROPERTY_OF = 'http://www.w3.org/2000/01/rdf-schema#subPropertyOf';
	private const OWL_CLASS = 'http://www.w3.org/2002/07/owl#Class';
	private const OWL_DATATYPE_PROPERTY = 'http://www.w3.org/2002/07/owl#DatatypeProperty';
	private const OWL_OBJECT_PROPERTY = 'http://www.w3.org/2002/07/owl#ObjectProperty';

	private $sparqlClient;
	private $cache;
	private $entitiesWithoutLabels = [];
	private $entityLabels = [];

	public function __construct( CacheInterface $cache ) {
		$this->sparqlClient = new SparqlClient();
		$this->cache = $cache;
	}

	/**
	 * @return ValidationMessage[]
	 */
	public function validate( string $vocabularyPrefix, string $vocabularyDefinitionUrl ): array {
		$messages = [];

		if ( !filter_var( $vocabularyPrefix, FILTER_VALIDATE_URL ) ) {
			$messages += [
				new ValidationMessage(
					ValidationMessage::LEVEL_ERROR,
					ValidationMessage::NO_GROUP,
					"{$vocabularyDefinitionUrl} is not a valid URL"
				)
			];
			return $messages;
		}

		$triples = $this->loadTriples( $vocabularyDefinitionUrl );
		$count = $triples->count();
		if ( $count === 0 ) {
			$messages += [
				new ValidationMessage(
					ValidationMessage::LEVEL_INFO,
					ValidationMessage::NO_GROUP,
					"No machine readable description of the {$vocabularyPrefix} vocabulary found. " .
					"Please set an URL to a RDF definition of the vocabulary " .
					"in the 'Vocabulary RDF definition' parameter."
				)
			];
			return $messages;
		}
		$messages += [
			new ValidationMessage(
				ValidationMessage::LEVEL_INFO,
				ValidationMessage::NO_GROUP,
				"{$count} triple extracted from the definition file {$vocabularyDefinitionUrl}"
			)
		];

		// We do checks for equivalent class
		// We first build a WD => external mapping
		// with error if there is a conflict and do some preliminary checks
		$entityToClassMapping = [];
		$classToEntityMapping = [];
		foreach ( $this->pairsForWikidataProperties( [ 'P1709' ], $vocabularyPrefix ) as $mapping ) {
			$uri = $mapping['uri'];
			$entity = $mapping['entity'];

			$this->validateIsBetweenClasses( $entity, $uri, $messages, $triples );

			// Multiple WD entities maps to the same thing
			if ( array_key_exists( $entity, $entityToClassMapping ) ) {
				$messages[] = new ValidationMessage(
					ValidationMessage::LEVEL_WARNING,
					ValidationMessage::GROUP_CLASS,
					"{$this->formatLink($entity)} is mapped as equivalent class to both " .
					"{$this->formatLink($entityToClassMapping[$entity])} and {$this->formatLink($uri)}."
				);
			}

			// The same WD entity maps to multiple things
			if ( array_key_exists( $uri, $classToEntityMapping ) ) {
				$messages[] = new ValidationMessage(
					ValidationMessage::LEVEL_WARNING,
					ValidationMessage::GROUP_CLASS,
					"{$this->formatLink($uri)} is mapped as equivalent class from both " .
					"{$this->formatLink($classToEntityMapping[$uri])} and {$this->formatLink($entity)}."
				);
			}

			$entityToClassMapping[$entity] = $uri;
			$classToEntityMapping[$uri] = $entity;
		}

		// We check that, if an element is subclass of an other in Wikidata,
		// it is sub class of in the ontology
		// TODO: make it do more than 1 hop
		$wdSubClassOfHierarchy = $this->wikidataDirectChildOf(
			array_keys( $entityToClassMapping ),
			'P279'
		);
		foreach ( $wdSubClassOfHierarchy as $map ) {
			$sub = $map['sub'];
			$super = $map['super'];
			if ( !$this->isInSuperHierarchy(
				$entityToClassMapping[$sub], $entityToClassMapping[$super],
				self::RDFS_SUB_CLASS_OF, $triples
			) ) {
				$messages[] = new ValidationMessage(
					ValidationMessage::LEVEL_WARNING,
					ValidationMessage::GROUP_CLASS_HIERARCHY,
					"{$this->formatLink($entityToClassMapping[$sub])} is not a sub class of " .
					"{$this->formatLink($entityToClassMapping[$super])} even if {$this->formatLink($sub)} " .
					"is a sub class of {$this->formatLink($super)}."
				);
			}
		}

		// We do checks for sub/super classes
		foreach ( $this->pairsForWikidataProperties( [ 'P3950' ], $vocabularyPrefix ) as $mapping ) {
			$this->validateIsBetweenClasses( $mapping['entity'], $mapping['uri'], $messages, $triples );
		}

		// We do checks for equivalent properties
		// We first build a WD => external mapping
		// with error if there is a conflict and do some preliminary checks
		$entityToPropertyMapping = [];
		$propertyToEntityMapping = [];
		foreach ( $this->pairsForWikidataProperties( [ 'P1628' ], $vocabularyPrefix ) as $mapping ) {
			$uri = $mapping['uri'];
			$entity = $mapping['entity'];

			$this->validateIsBetweenProperty( $entity, $uri, $messages, $triples );

			// Multiple WD entities maps to the same thing
			if ( array_key_exists( $entity, $entityToPropertyMapping ) ) {
				$messages[] = new ValidationMessage(
					ValidationMessage::LEVEL_WARNING,
					ValidationMessage::GROUP_PROPERTY,
					"{$this->formatLink($entity)} is mapped as equivalent property to both " .
					"{$this->formatLink($entityToPropertyMapping[$entity])} and {$this->formatLink($uri)}."
				);
			}

			// The same WD entity maps to multiple things
			if ( array_key_exists( $uri, $propertyToEntityMapping ) ) {
				$messages[] = new ValidationMessage(
					ValidationMessage::LEVEL_WARNING,
					ValidationMessage::GROUP_PROPERTY,
					"{$this->formatLink($uri)} is mapped as equivalent property from both " .
					"{$this->formatLink($propertyToEntityMapping[$uri])} and {$this->formatLink($entity)}."
				);
			}

			$entityToPropertyMapping[$entity] = $uri;
			$propertyToEntityMapping[$uri] = $entity;
		}

		// We check that, if an element is subproperty of an other in Wikidata,
		// it is sub property of it in the ontology
		// TODO: make it do more than 1 hop
		$wdSubPropertyOfHierarchy = $this->wikidataDirectChildOf(
			array_keys( $entityToPropertyMapping ),
			'P1647'
		);
		foreach ( $wdSubPropertyOfHierarchy as $map ) {
			$sub = $map['sub'];
			$super = $map['super'];
			if ( !$this->isInSuperHierarchy(
				$entityToPropertyMapping[$sub], $entityToPropertyMapping[$super],
				self::RDFS_SUB_PROPERTY_OF, $triples
			) ) {
				$messages[] = new ValidationMessage(
					ValidationMessage::LEVEL_WARNING,
					ValidationMessage::GROUP_PROPERTY_HIERARCHY,
					"{$this->formatLink($entityToPropertyMapping[$sub])} is not a sub property of " .
					"{$this->formatLink($entityToPropertyMapping[$super])} even if {$this->formatLink($sub)} " .
					"is a sub property of {$this->formatLink($super)}."
				);
			}
		}

		// We do checks for sub/super properties
		foreach ( $this->pairsForWikidataProperties(
			[ 'P2235', 'P2236' ], $vocabularyPrefix
		) as $mapping ) {
			$this->validateIsBetweenProperty( $mapping['entity'], $mapping['uri'], $messages, $triples );
		}

		// We do checks for exact match properties
		$entityToUriMatchMapping = [];
		$uriToEntityMatchMapping = [];
		$allVocabularyTerms = $triples->subjects();
		// TODO: load all the exact matches to the external vocabulary
		foreach ( $this->pairsForWikidataPropertiesAndValues(
			[ 'P2888' ], $allVocabularyTerms
		) as $mapping ) {
			$uri = $mapping['uri'];
			$entity = $mapping['entity'];
			// We check that all target of property mappings are not classes or properties
			if ( $this->isProperty( $uri, $triples ) ) {
				if ( $this->isWbProperty( $entity ) ) {
					$messages[] = new ValidationMessage(
						ValidationMessage::LEVEL_WARNING,
						ValidationMessage::GROUP_CONCEPT,
						"{$this->formatLink($uri)} is a property mapped as an " .
						"<a href=\"https://www.wikidata.org/wiki/Property:P2888\">exact match (P2888)</a> of " .
						"{$this->formatLink($entity)}</a> which is a property. You may want to use " .
						"<a href=\"https://www.wikidata.org/wiki/Property:P1628\">equivalent property (P1628)</a> " .
						"instead."
					);
				} elseif ( !$this->isWbItem( $entity ) ) {
					$messages[] = new ValidationMessage(
						ValidationMessage::LEVEL_WARNING,
						ValidationMessage::GROUP_CONCEPT,
						"{$this->formatLink($mapping['uri'])} is not mapped " .
						"to a Wikidata item but to {$this->formatLink($mapping['entity'])}</a>."
					);
				}
			} elseif ( $this->isClass( $uri, $triples ) ) {
				if ( $this->isWbItem( $entity ) ) {
					$messages[] = new ValidationMessage(
						ValidationMessage::LEVEL_WARNING,
						ValidationMessage::GROUP_CONCEPT,
						"{$this->formatLink($uri)} is a class mapped as an " .
						"<a href=\"https://www.wikidata.org/wiki/Property:P2888\">exact match (P2888)</a> of " .
						"{$this->formatLink($entity)}</a> which is an item. You may want to use " .
						"<a href=\"https://www.wikidata.org/wiki/Property:P1709\">equivalent class (P1709)</a> " .
						"instead."
					);
				} else {
					$messages[] = new ValidationMessage(
						ValidationMessage::LEVEL_WARNING,
						ValidationMessage::GROUP_CONCEPT,
						"{$this->formatLink($uri)} is a class mapped as an " .
						"<a href=\"https://www.wikidata.org/wiki/Property:P2888\">exact match (P2888)</a> of " .
						"{$this->formatLink($entity)}</a> which is not an item."
					);
				}
			} elseif ( !$this->isWbItem( $mapping['entity'] ) ) {
				/* We check that all target of exact match mappings are used on item entities
				TODO: controversial, disabled for now
				$messages[] = new ValidationMessage(
					ValidationMessage::LEVEL_WARNING,
					ValidationMessage::GROUP_CONCEPT,
					"{$this->formatLink($mapping['uri'])} is not mapped " .
					"to a Wikidata item but to {$this->formatLink($mapping['entity'])}</a>."
				);*/
			}

			// Multiple WD entities maps to the same thing
			if ( array_key_exists( $entity, $entityToUriMatchMapping ) ) {
				$messages[] = new ValidationMessage(
					ValidationMessage::LEVEL_WARNING,
					ValidationMessage::GROUP_CONCEPT,
					"{$this->formatLink($entity)} is mapped as exact match to both " .
					"{$this->formatLink($entityToUriMatchMapping[$entity])} and {$this->formatLink($uri)}."
				);
			}
			if (
				array_key_exists( $entity, $entityToPropertyMapping ) &&
				$entityToPropertyMapping[$entity] !== $uri
			) {
				$messages[] = new ValidationMessage(
					ValidationMessage::LEVEL_WARNING,
					ValidationMessage::GROUP_CONCEPT_AND_CLASS_PROPERTY,
					"{$this->formatLink($entity)} is mapped as exact match to " .
					"{$this->formatLink($uri)} " .
					"and as equivalent property to {$this->formatLink($entityToPropertyMapping[$entity])}."
				);
			}
			if (
				array_key_exists( $entity, $entityToClassMapping ) &&
				$entityToClassMapping[$entity] !== $uri
			) {
				$messages[] = new ValidationMessage(
					ValidationMessage::LEVEL_WARNING,
					ValidationMessage::GROUP_CONCEPT_AND_CLASS_PROPERTY,
					"{$this->formatLink($entity)} is mapped as exact match to " .
					"{$this->formatLink($uri)} " .
					"and as equivalent class to {$this->formatLink($entityToClassMapping[$entity])}."
				);
			}

			// The same WD entity maps to multiple things
			if ( array_key_exists( $uri, $uriToEntityMatchMapping ) ) {
				$messages[] = new ValidationMessage(
					ValidationMessage::LEVEL_WARNING,
					ValidationMessage::GROUP_CONCEPT,
					"{$this->formatLink($entity)} is mapped as exact match from both " .
					"{$this->formatLink($uriToEntityMatchMapping[$uri])} and {$this->formatLink($entity)}."
				);
			}
			if (
				array_key_exists( $uri, $propertyToEntityMapping ) &&
				$propertyToEntityMapping[$uri] !== $entity
			) {
				$messages[] = new ValidationMessage(
					ValidationMessage::LEVEL_WARNING,
					ValidationMessage::GROUP_CONCEPT_AND_CLASS_PROPERTY,
					"{$this->formatLink($uri)} is mapped as exact match from " .
					"{$this->formatLink($entity)} " .
					"and as equivalent property from {$this->formatLink($propertyToEntityMapping[$uri])}."
				);
			}
			if (
				array_key_exists( $uri, $classToEntityMapping ) &&
				$classToEntityMapping[$uri] !== $entity
			) {
				$messages[] = new ValidationMessage(
					ValidationMessage::LEVEL_WARNING,
					ValidationMessage::GROUP_CONCEPT_AND_CLASS_PROPERTY,
					"{$this->formatLink($uri)} is mapped as exact match from " .
					"{$this->formatLink($entity)} " .
					"and as equivalent class from {$this->formatLink($classToEntityMapping[$uri])}."
				);
			}

			$entityToUriMatchMapping[$entity] = $uri;
			$uriToEntityMatchMapping[$uri] = $entity;
		}

		if ( !$this->withErrorOrWarning( $messages ) ) {
			$messages[] = new ValidationMessage(
				ValidationMessage::LEVEL_SUCCESS,
				ValidationMessage::NO_GROUP,
				"Mapping have been validated without any error or warning found"
			);
		}

		$this->loadLabels();
		return $this->formatMessages( $messages );
	}

	private function loadTriples( $documentUri ) {
		$key = 'tptools-mapping-' . md5( $documentUri );
		$triples = $this->cache->get( $key );
		if ( $triples === null ) {
			$triples = $this->doLoadTriples( $documentUri );
			$this->cache->set( $key, $triples, 60 * 60 * 24 );
		}
		return new SimpleTripleStore( $triples );
	}

	private function doLoadTriples( $documentUri ) {
		if ( strpos( $documentUri, 'http://' ) === 0 ) {
			// Hack to avoid compatibility problems between ARC2 and HSTS
			$documentUris = [ str_replace( 'http://', 'https://', $documentUri ), $documentUri ];
		} else {
			$documentUris = [ $documentUri ];
		}
		foreach ( $documentUris as $documentUri ) {
			$parser = ARC2::getRDFParser();
			$parser->parse( $documentUri );
			$triples = $parser->getTriples();
			if ( !empty( $triples ) ) {
				return $triples;
			}
		}
		return [];
	}

	private function validateIsBetweenClasses(
		$entity, $uri, &$messages, SimpleTripleStore $triples
	) {
		// We check that all equivalent classes have the rdfs:Class type
		if ( !$this->isClass( $uri, $triples ) ) {
			$messages[] = new ValidationMessage(
				ValidationMessage::LEVEL_ERROR,
				ValidationMessage::GROUP_CLASS,
				"{$this->formatLink($uri)} is not an instance of <code>rdfs:Class</code> or " .
				"<code>owl:Class</code> but is mapped with {$this->formatLink($entity)}."
			);
		}

		// We check that all equivalent classes are used on item entities
		if ( !$this->isWbItem( $entity ) ) {
			$messages[] = new ValidationMessage(
				ValidationMessage::LEVEL_ERROR,
				ValidationMessage::GROUP_CLASS,
				"{$this->formatLink($entity)} is not a Wikidata item but maps " .
				"to the class {$this->formatLink($uri)}."
			);
		}
	}

	private function validateIsBetweenProperty(
		$entity, $uri, &$messages, SimpleTripleStore $triples
	) {
		// We check that all target of property mappings have the rdf:Property type
		if ( !$this->isProperty( $uri, $triples ) ) {
			$messages[] = new ValidationMessage(
				ValidationMessage::LEVEL_ERROR,
				ValidationMessage::GROUP_PROPERTY,
				"{$this->formatLink($uri)} is not an instance of <code>rdf:Property</code>, " .
				"<code>owl:DatatypeProperty</code> or <code>owl:ObjectProperty</code> but is mapped with " .
				"{$this->formatLink($entity)}."
			);
		}

		// We check that all target of property mappings are used on property entities
		if ( !$this->isWbProperty( $entity ) ) {
			$messages[] = new ValidationMessage(
				ValidationMessage::LEVEL_ERROR,
				ValidationMessage::GROUP_PROPERTY,
				"{$this->formatLink($entity)} is not a Wikidata property but maps " .
				"to the property {$this->formatLink($uri)}."
			);
		}
	}

	private function pairsForWikidataProperties( $propertyIds, string $vocabularyPrefix ) {
		$propertyPath = implode( '|', array_map( static function ( $propertyId ) {
			return 'wdt:' . $propertyId;
		}, $propertyIds ) );
		if ( strpos( $propertyPath, '|' ) ) {
			$propertyPath = '(' . $propertyPath . ')';
		}
		return $this->sparqlClient->getTuples(
			'SELECT DISTINCT ?entity ?uri WHERE {' .
			'  hint:Query hint:optimizer "None" . ' .
			'  ?entity ' . $propertyPath . ' ?uri .' .
			'  FILTER(STRSTARTS(STR(?uri), "' . $vocabularyPrefix . '"))' .
			'}'
		);
	}

	private function pairsForWikidataPropertiesAndValues( $propertyIds, array $values ) {
		$propertyPath = implode( '|', array_map( static function ( $propertyId ) {
			return 'wdt:' . $propertyId;
		}, $propertyIds ) );
		if ( strpos( $propertyPath, '|' ) ) {
			$propertyPath = '(' . $propertyPath . ')';
		}
		$values = implode( ' ', array_map( static function ( $uri ) {
			return '<' . $uri . '>';
		}, $values ) );
		return $this->sparqlClient->getTuples(
			'SELECT DISTINCT ?entity ?uri WHERE {' .
			'  hint:Query hint:optimizer "None" . ' .
			'  VALUES ?uri { ' . $values . ' } ' .
			'  ?entity ' . $propertyPath . ' ?uri .' .
			'}'
		);
	}

	private function wikidataDirectChildOf( $itemUris, $propertyId ) {
		$values = implode( ' ', array_map( static function ( $uri ) {
			return '<' . $uri . '>';
		}, $itemUris ) );
		return $this->sparqlClient->getTuples(
			'SELECT DISTINCT ?sub ?super WHERE {' .
			'  hint:Query hint:optimizer "None" . ' .
			'  VALUES ?sub { ' . $values . '}' .
			'  ?sub wdt:' . $propertyId . ' ?super .' .
			'  VALUES ?super { ' . $values . '}' .
			'}'
		);
	}

	private function isInSuperHierarchy(
		$start, $end, $relation,
		SimpleTripleStore $triples, &$seen = []
	) {
		if ( $start === $end ) {
			return true;
		}
		if ( in_array( $start, $seen ) ) {
			return false;
		}
		$seen[] = $start;
		foreach ( $triples->objects( $start, $relation ) as $parentStart ) {
			if ( $this->isInSuperHierarchy( $parentStart, $end, $relation, $triples, $seen ) ) {
				return true;
			}
		}
		return false;
	}

	private function formatLink( $uri ) {
		if ( strpos( $uri, 'http://www.wikidata.org/entity/' ) === 0 ) {
			$id = str_replace( 'http://www.wikidata.org/entity/', '', $uri );
			$this->entitiesWithoutLabels[$id] = $id;
			return "$$id$";
		}
		return "<a href='{$uri}'>{$uri}</a>";
	}

	/**
	 * @param ValidationMessage[] $messages
	 * @return bool
	 */
	private function withErrorOrWarning( $messages ) {
		foreach ( $messages as $message ) {
			switch ( $message->getLevel() ) {
				case ValidationMessage::LEVEL_ERROR:
				case ValidationMessage::LEVEL_WARNING:
					return true;
			}
		}
		return false;
	}

	private function isClass( $uri, SimpleTripleStore $triples ) {
		return $triples->contains( $uri, self::RDF_TYPE, self::RDFS_CLASS ) ||
			$triples->contains( $uri, self::RDF_TYPE, self::OWL_CLASS );
	}

	private function isProperty( $uri, SimpleTripleStore $triples ) {
		return $triples->contains( $uri, self::RDF_TYPE, self::RDF_PROPERTY ) ||
			$triples->contains( $uri, self::RDF_TYPE, self::OWL_DATATYPE_PROPERTY ) ||
			$triples->contains( $uri, self::RDF_TYPE, self::OWL_OBJECT_PROPERTY );
	}

	private function isWbItem( $uri ) {
		return strpos( $uri, 'http://www.wikidata.org/entity/Q' ) === 0;
	}

	private function isWbProperty( $uri ) {
		return strpos( $uri, 'http://www.wikidata.org/entity/P' ) === 0;
	}

	private function formatMessages( array $messages ) {
		$messages = array_map( function ( ValidationMessage $message ) {
			return new ValidationMessage(
				$message->getLevel(),
				$message->getGroup(),
				preg_replace_callback( '/\$([PQ]\d+)\$/', function ( $args ) {
					$entityId = $args[1];
					if ( array_key_exists( $entityId, $this->entityLabels ) ) {
						return "<a href='https://www.wikidata.org/entity/{$entityId}'>
									{$this->entityLabels[$entityId]} ({$entityId})
								</a>";
					}
					return $args[1];
				}, $message->getMessage() )
			);
		}, $messages );
		usort( $messages, static function ( ValidationMessage $a, ValidationMessage $b ) {
			return ValidationMessage::$PRIORITIES[$b->getLevel()] -
				ValidationMessage::$PRIORITIES[$a->getLevel()];
		} );
		return $messages;
	}

	private function loadLabels() {
		$entitiesToGet = [];
		foreach ( $this->entitiesWithoutLabels as $entityId ) {
			$label = $this->cache->get( 'tptools-entity-label-en-' . $entityId );
			if ( $label !== null ) {
				$this->entityLabels[$entityId] = $label;
			} else {
				$entitiesToGet[] = $entityId;
			}
		}

		if ( !empty( $entitiesToGet ) ) {
			$values = implode( ' ', array_map( static function ( $id ) {
				return 'wd:' . $id;
			}, $entitiesToGet ) );
			foreach ( $this->sparqlClient->getTuples(
				'SELECT ?entity ?entityLabel WHERE {' .
				'  VALUES ?entity { ' . $values . ' }' .
				'  SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }' .
				'}'
			) as $tuple ) {
				$entityId = str_replace( 'http://www.wikidata.org/entity/', '', $tuple['entity'] );
				$label = $tuple['entityLabel'];
				$this->cache->set( 'tptools-entity-label-en-' . $entityId, $label, 60 * 60 * 24 * 30 );
				$this->entityLabels[$entityId] = $label;
			}
		}
	}
}

<?php

namespace Tptools\MappingValidator;

class ValidationMessage {

	public static $PRIORITIES = [
		self::LEVEL_SUCCESS => 3,
		self::LEVEL_ERROR => 2,
		self::LEVEL_WARNING => 1,
		self::LEVEL_INFO => 0
	];

	public const LEVEL_SUCCESS = 'success';
	public const LEVEL_WARNING = 'warning';
	public const LEVEL_ERROR = 'danger';
	public const LEVEL_INFO = 'info';

	public const GROUP_CLASS = 'class';
	public const GROUP_CLASS_HIERARCHY = 'classHierarchy';
	public const GROUP_PROPERTY = 'property';
	public const GROUP_PROPERTY_HIERARCHY = 'propertyHierarchy';
	public const GROUP_CONCEPT = 'concept';
	public const GROUP_CONCEPT_AND_CLASS_PROPERTY = 'conceptAndClassProperty';
	public const NO_GROUP = 'no';

	private $level;
	private $group;
	private $message;

	public function __construct( string $level, string $group, string $message ) {
		$this->level = $level;
		$this->group = $group;
		$this->message = $message;
	}

	public function getLevel(): string {
		return $this->level;
	}

	public function getGroup(): string {
		return $this->group;
	}

	public function getMessage(): string {
		return $this->message;
	}
}

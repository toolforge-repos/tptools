<?php

namespace Tptools\MappingValidator;

use ARC2;
use Countable;

class SimpleTripleStore implements Countable {

	private $spo;

	public function __construct( $triples ) {
		$this->spo = ARC2::getSimpleIndex( $triples );
	}

	public function contains( $subject, $predicate, $object ) {
		return array_key_exists( $subject, $this->spo ) &&
			array_key_exists( $predicate, $this->spo[$subject] ) &&
			in_array( $object, $this->spo[$subject][$predicate] );
	}

	public function subjects() {
		return array_keys( $this->spo );
	}

	public function objects( $subject, $predicate ) {
		if ( !array_key_exists( $subject, $this->spo ) ) {
			return [];
		}
		if ( !array_key_exists( $predicate, $this->spo[$subject] ) ) {
			return [];
		}
		return $this->spo[$subject][$predicate];
	}

	public function count() {
		$count = 0;
		foreach ( $this->spo as $po ) {
			foreach ( $po as $os ) {
				$count += count( $os );
			}
		}
		return $count;
	}

	public function isEmpty() {
		foreach ( $this->spo as $po ) {
			foreach ( $po as $os ) {
				if ( !empty( $os ) ) {
					return false;
				}
			}
		}
		return true;
	}
}

<?php

ini_set( 'display_errors', 1 );
ini_set( 'display_startup_errors', 1 );
error_reporting( E_ALL );

use Cache\Adapter\PHPArray\ArrayCachePool;
use Cache\Adapter\Redis\RedisCachePool;
use Tptools\MappingValidator\MappingValidator;
use Tptools\MappingValidator\ValidationMessage;
use Tptools\SparqlClient;

include __DIR__ . '/../vendor/autoload.php';

$cache = new ArrayCachePool();
if(class_exists('Redis')) {
	$client = new Redis();
	$client->connect('tools-redis', 6379);
	$pool = new RedisCachePool($client);
}

// Vocabularies URIs from https://lov.linkeddata.es
$knownVocabularyDefinitionUrls = [
	'http://dbpedia.org/ontology/' => 'http://downloads.dbpedia.org/2016-10/dbpedia_2016-10.nt',
	'http://data.doremus.org/ontology#' => 'https://raw.githubusercontent.com/DOREMUS-ANR/doremus-ontology/master/doremus.ttl',
	'http://dati.camera.it/ocd/' => 'http://dati.camera.it/ocd/classi.rdf',
	'http://dati.isprambiente.it/ontology/core#' => 'http://dati.isprambiente.it/ontology/core/ispra-core.0.1.rdf',
	'http://datos.gob.es/def/sector-publico/organizacion#' => 'http://datos.gob.es/sites/default/files/OntologiaDIR3/orges.owl',
	'http://linkedscience.org/teach/ns#' => 'http://www.w3.org/2012/pyRdfa/extract?uri=http%3A%2F%2Flinkedscience.org%2Fteach%2Fns&format=turtle',
	'http://ontology.cybershare.utep.edu/dbowl' => 'http://ontology.cybershare.utep.edu/dbowl/relational-to-ontology-mapping-primitive.owl',
	'http://privatealpha.com/ontology/certification/1#' => 'http://privatealpha.com/ontology/certification/1.n3',
	'http://purl.org/cwmo/#' => 'https://gabriel-alex.github.io/cwmo/CWMO-042.owl',
	'http://purl.org/dita/ns#' => 'https://raw.githubusercontent.com/ColinMaudry/dita-rdf/master/dita.ttl',
	'http://purl.org/gen/0.1#' => 'https://raw.githubusercontent.com/joshhansen/vocab-gen/master/releases/gen01.rdf',
	'http://purl.org/innovation/ns' => 'http://www.lexicater.co.uk/vocabularies/innovation/ns.owl',
	'http://purl.org/iot/vocab/m3-lite#' => 'http://sensormeasurement.appspot.com/ont/m3/m3-lite.owl',
	'http://purl.org/linguistics/gold' => 'http://linguistics-ontology.org/gold-2010.owl',
	'http://purl.org/linked-data/api/vocab#' => 'https://raw.githubusercontent.com/UKGovLD/linked-data-api/master/vocab/api.ttl',
	'http://purl.org/net/opmv/ns#' => 'http://www.w3.org/2007/08/pyRdfa/extract?uri=http://open-biomed.sourceforge.net/opmv/ns.html',
	'http://purl.org/ontology/daia' => 'https://github.com/gbv/daia/blob/implementations/schemas/daia.owl.n3',
	'http://purl.org/ontology/ecpo' => 'http://cklee.github.io/ecpo/ecpo.ttl',
	'http://purl.org/ontology/holding' => 'http://dini-ag-kim.github.io/holding-ontology/holding.ttl',
	'http://purl.org/ontology/olo/core#' => 'http://smiy.sourceforge.net/olo/rdf/orderedlistontology.owl',
	'http://purl.org/ontology/service' => 'http://dini-ag-kim.github.io/service-ontology/service.ttl',
	'http://purl.org/ontology/ssso' => 'http://gbv.github.io/ssso/ssso.ttl',
	'http://purl.org/rss/1.0' => 'http://web.resource.org/rss/1.0/schema.rdf',
	'http://purl.org/vocab/participation/schema' => 'http://vocab.org/participation/schema',
	'http://purl.org/voc/summa/' => 'http://people.aifb.kit.edu/ath/summa/',
	'http://purl.org/vso/ns' => 'http://www.heppnetz.de/ontologies/vso/ns.owl',
	'http://purl.org/wf4ever/wfdesc' => 'https://raw.githubusercontent.com/wf4ever/ro/master/wfdesc.owl',
	'http://rdf.geospecies.org/ont/geospecies' => 'http://rdf.geospecies.org/ont/geospecies.owl',
	'http://rdfunit.aksw.org/ns/core#' => 'http://rdfunit.aksw.org/ns/core.ttl',
	'http://schema.org/' => 'https://schema.org/version/latest/all-layers.ttl',
	'http://schema.theodi.org/odrs' => 'http://schema.theodi.org/odrs/index.ttl',
	'https://data.nasa.gov/ontologies/atmonto/ATM#' => 'https://data.nasa.gov/ontologies/atmonto/ATM.ttl',
	'https://data.nasa.gov/ontologies/atmonto/data#' => 'https://data.nasa.gov/ontologies/atmonto/data.ttl',
	'https://data.nasa.gov/ontologies/atmonto/equipment#' => 'https://data.nasa.gov/ontologies/atmonto/equipment.ttl',
	'https://data.nasa.gov/ontologies/atmonto/general#' => 'https://data.nasa.gov/ontologies/atmonto/general.ttl',
	'https://data.nasa.gov/ontologies/atmonto/NAS#' => 'https://data.nasa.gov/ontologies/atmonto/NAS.ttl',
	'http://softeng.polito.it/rsctx' => 'http://softeng.polito.it/rsctx/rsctx.rdf',
	'https://w3id.org/saref4ee' => 'http://ontology.tno.nl/saref4ee.owl',
	'https://w3id.org/saref' => 'http://ontology.tno.nl/saref.owl',
	'http://www.europeana.eu/schemas/edm/' => 'http://www.europeana.eu/schemas/edm/rdf/edm.owl',
	'http://www.loc.gov/mads/rdf/v1' => 'http://www.loc.gov/standards/mads/rdf/v1.rdf',
	'http://www.observedchange.com/tisc/ns#' => 'http://www.w3.org/2012/pyRdfa/extract?uri=http://observedchange.com/tisc/ns',
	'http://www.openarchives.org/ore/terms/' => 'http://www.openarchives.org/ore/1.0/terms',
	'http://www.w3.org/1999/xhtml/vocab' => 'http://www.w3.org/2012/pyRdfa/extract?uri=http%3A%2F%2Fwww.w3.org%2F1999%2Fxhtml%2Fvocab',
	'http://www.w3.org/2003/11/swrl' => 'http://www.w3.org/Submission/2004/SUBM-SWRL-20040521/swrl.rdf',
	'http://www.w3.org/2004/02/skos/core' => 'http://www.w3.org/2009/08/skos-reference/skos.rdf',
	'http://www.w3.org/ns/regorg' => 'http://www.w3.org/TR/vocab-regorg/',
	'http://www.w3.org/ns/sparql-service-description' => 'http://www.w3.org/ns/sparql-service-description.rdf',
	'http://www.wiwiss.fu-berlin.de/suhl/bizer/D2RQ/0.1' => 'http://d2rq.org/terms/d2rq.ttl',
	'http://xmlns.com/wot/0.1/' => 'http://xmlns.com/wot/0.1/index.rdf'
];

$vocabularyPrefix = array_key_exists('prefix', $_GET) ? $_GET['prefix'] : '';
$vocabularyDefinitionUrl = array_key_exists('vocabularyDefinition', $_GET) && $_GET['vocabularyDefinition']
	? $_GET['vocabularyDefinition']
	: (array_key_exists($vocabularyPrefix, $knownVocabularyDefinitionUrls)
		? $knownVocabularyDefinitionUrls[$vocabularyPrefix]
		: $vocabularyPrefix);

$existingVocabularyPrefixes = $cache->get('tptools-mapping-existingVocabularyPrefixes');
if($existingVocabularyPrefixes === null) {
	$existingVocabularyPrefixes = json_encode(array_map(
		function ($tuple) {
			return $tuple['prefix'];
		},
		(new SparqlClient())->getTuples(
			'SELECT ?prefix WHERE {' .
			'  ?item wdt:P1628|wdt:P1709|wdt:P2235|wdt:P2236 ?uri .' .
			'  BIND(REPLACE(STR(?uri), "[^/#]+$", "") AS ?prefix)' .
			'} GROUP BY ?prefix ORDER BY DESC(COUNT(?uri))'
		)
	));
	$cache->set('tptools-mapping-existingVocabularyPrefixes', $existingVocabularyPrefixes, 60*60*24);
}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Validator for mappings to external vocabularies stored in Wikidata">
	<meta name="author" content="Tpt">

	<title>Wikidata Mapping Validator</title>

	<link href="https://tools-static.wmflabs.org/cdnjs/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css"
		  rel="stylesheet">
	<link href="style.css" rel="stylesheet">
	<link href="typeaheadjs.css" rel="stylesheet">
</head>
<div class="container">
	<header class="header clearfix">
		<nav>
			<ul class="nav nav-pills float-right">
				<li class="nav-item">
					<a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="wdql.html">GraphiQL</a>
				</li>
				<li class="nav-item">
					<a class="nav-link active" href="#">Wikidata Mapping Validator</a>
				</li>
			</ul>
		</nav>
		<h3 class="text-muted">Tptools</h3>
	</header>

	<main role="main">
		<div class="jumbotron">
			<form action="wd_mapping_validator.php" method="get">
				<div class="form-group">
					<label for="prefix">Vocabulary prefix</label>
					<input name="prefix" type="url" class="form-control" id="prefix" aria-describedby="prefixHelp"
						   required value="<?php echo htmlspecialchars($vocabularyPrefix); ?>">
					<small id="prefixHelp" class="form-text text-muted">Vocabulary prefix like "http://schema.org/".
					</small>
				</div>
				<div class="form-group">
					<label for="vocabularyDefinition">Vocabulary RDF definition</label>
					<input name="vocabularyDefinition" type="url" class="form-control" id="vocabularyDefinition"
						   aria-describedby="vocabularyDefinitionHelp"
						   value="<?php echo htmlspecialchars($vocabularyDefinitionUrl); ?>">
					<small id="vocabularyDefinitionHelp" class="form-text text-muted">URL of an RDF file defining the
						vocabulary. If empty the vocabulary prefix is going to be used.
					</small>
				</div>
				<input type="submit" value="Validate" class="btn btn-primary"/>
			</form>
		</div>

		<div class="row content">
			<?php
			if ($vocabularyPrefix !== '') {
				$validator = new MappingValidator( $cache );
				$groups = [
				    ValidationMessage::NO_GROUP => [
				        'messages' => []
                    ],
					ValidationMessage::GROUP_CLASS => [
						'label' => 'Classes mapping report',
						'messages' => []
					],
					ValidationMessage::GROUP_CLASS_HIERARCHY => [
						'label' => 'Classes hierarchy report',
						'messages' => []
					],
					ValidationMessage::GROUP_PROPERTY => [
						'label' => 'Properties mapping report',
						'messages' => []
					],
					ValidationMessage::GROUP_PROPERTY_HIERARCHY => [
						'label' => 'Properties hierarchy report',
						'messages' => []
					],
					ValidationMessage::GROUP_CONCEPT => [
						'label' => 'Concepts mapping report',
						'messages' => []
					],
                    ValidationMessage::GROUP_CONCEPT_AND_CLASS_PROPERTY => [
						'label' => 'Concepts and class/properties mapping conflicts report',
						'messages' => []
					]
                ];
				foreach ($validator->validate($vocabularyPrefix, $vocabularyDefinitionUrl) as $message) {
				    $groups[$message->getGroup()]['messages'][] = $message;
                }
				foreach ($groups[ValidationMessage::NO_GROUP]['messages'] as $result) {
					echo "<div class='alert alert-{$result->getLevel()}'>{$result->getMessage()}</div>";
				}
				echo '<div class="accordion" id="resultsAccordion" style="width: 100%;">';
				foreach ($groups as $key => $group) {
				    $messages = $group['messages'];
				    if(empty($group['messages']) || $key === ValidationMessage::NO_GROUP) {
				        continue;
                    }
					$errors = count(array_filter($messages, function (ValidationMessage $message) {
						return $message->getLevel() === ValidationMessage::LEVEL_ERROR;
					}));
					$warnings = count(array_filter($messages, function (ValidationMessage $message) {
						return $message->getLevel() === ValidationMessage::LEVEL_WARNING;
					}));
					echo "<div class='card'>";
					echo "<div class='card-header'><h2 id='{$key}heading'><button class='btn btn-link' type='button' data-toggle='collapse' data-target='#collapse{$key}' aria-controls='collapse{$key}'>";
					echo "{$group['label']} ($errors errors, $warnings warnings)";
					echo "</button></h2></div>";
					echo "<div id='collapse{$key}' class='collapse' aria-labelledby='{$key}heading' data-parent='#resultsAccordion'><div class='card-body'>";
					foreach ($messages as $result) {
						echo "<div class='alert alert-{$result->getLevel()}'>{$result->getMessage()}</div>";
					}
					echo "</div></div></div>";
                }
				echo '</div>';
			}
			?>
		</div>
	</main>

    <aside class="card">
        <div class="card-body">
            <h2 class="card-title">Help</h2>
            <div class="card-text">
                <p>This tool performs the following checks:</p>
                <ul>
                    <li>All usages of <a href="https://www.wikidata.org/wiki/Property:P1709">equivalent class (P1709)</a> must be on items and connect to instances of <code>rdfs:Class</code> or <code>owl:Class</code> in the vocabulary description.</li>
                    <li>All usages of <a href="https://www.wikidata.org/wiki/Property:P1628">equivalent property (P1628)</a>, <a href="https://www.wikidata.org/wiki/Property:P2235">external superproperty (P2235)</a>, and <a href="https://www.wikidata.org/wiki/Property:P2236">external subproperty (P2236)</a> must be on property entities and connect to instances of <code>rdf:Property</code>, <code>owl:ObjectProperty</code>, or <code>owl:DatatypeProperty</code> in the vocabulary description.</li>
                    <li>All usages of <a href="https://www.wikidata.org/wiki/Property:P2888">exact match (P2888)</a> connecting to instances of <code>rdfs:Class</code> or <code>owl:Class</code> in the vocabulary description may use <a href="https://www.wikidata.org/wiki/Property:P1709">equivalent class (P1709)</a> instead.</li>
                    <li>All usages of <a href="https://www.wikidata.org/wiki/Property:P2888">exact match (P2888)</a> connecting to instances of <code>rdf:Property</code>, <code>owl:ObjectProperty</code>, or <code>owl:DatatypeProperty</code> in the vocabulary description may use <a href="https://www.wikidata.org/wiki/Property:P1628">equivalent property (P1628)</a> instead.</li>
                    <li>All usages of <a href="https://www.wikidata.org/wiki/Property:P2888">exact match (P2888)</a> should be on items.</li>
                    <li>All resources defined in the vocabulary description should be connected to at most one Wikidata entity.</li>
                    <li>If two Wikidata classes are connected with <a href="https://www.wikidata.org/wiki/Property:P279">sub class of (P279)</a>, then the vocabulary classes they are mapped to should also be connected with <code>rdfs:subClassOf</code>.</li>
                    <li>If two Wikidata properties are connected with <a href="https://www.wikidata.org/wiki/Property:P1647">sub property of (P1647)</a>, then the vocabulary properties they are mapped to should also be connected with <code>rdfs:subPropertyOf</code>.</li>
                </ul>
                <p>Warning: on big vocabularies this tools executes a lot of SPARQL queries and may be slow to load.</p>
            </div>
        </div>
    </aside>

	<footer class="footer">
		<p>
			Code licensed <a
					href="https://phabricator.wikimedia.org/source/tool-tptools/browse/master/LICENSE">GPLv2+</a>
			<a href="https://tools.wmflabs.org/admin/">
				<img src="https://tools-static.wmflabs.org/toolforge/banners/Powered-by-Toolforge.png"
					 alt="Powered by Toolforge">
			</a>
		</p>
	</footer>
</div>
<script src="https://tools-static.wmflabs.org/cdnjs/ajax/libs/jquery/3.3.1/jquery.slim.min.js"></script>
<script src="https://tools-static.wmflabs.org/cdnjs/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
<script src="https://tools-static.wmflabs.org/cdnjs/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script>

	$('#prefix').typeahead({
			highlight: true
		},
		{
			name: 'prefixes',
			source: new Bloodhound({
				datumTokenizer: Bloodhound.tokenizers.nonword,
				queryTokenizer: Bloodhound.tokenizers.nonword,
				local: <?php echo $existingVocabularyPrefixes ?>
			}),
			limit: 20
		}).change(function () {
		$('#vocabularyDefinition').val('')
	})
</script>
</body>
</html>

